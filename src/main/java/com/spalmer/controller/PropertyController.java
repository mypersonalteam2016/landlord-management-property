package com.spalmer.controller;

import com.spalmer.model.PropertyDTO;
import com.spalmer.service.PropertyService;
import com.spalmer.user.model.response.ResponseDTO;
import com.spalmer.user.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.List;

@RestController
public class PropertyController {

	private PropertyService propertyService;

	@Autowired
	public PropertyController(PropertyService propertyService) {
		this.propertyService = propertyService;
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ResponseDTO getProperties(Principal principal) {

		List<PropertyDTO> properties = propertyService.getProperties(getUser(principal));

		return ResponseDTO.builder()
				.response(properties)
				.success(Boolean.TRUE).build();
	}

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public ResponseDTO addProperty(@RequestBody PropertyDTO dto, Principal principal) {
		User user = getUser(principal);

		dto.setUserId(user.getId());

		propertyService.addProperty(dto);

		return ResponseDTO.builder()
				.response(dto)
				.success(Boolean.TRUE).build();
	}

	private User getUser(Principal principal) {
		return (User) ((UsernamePasswordAuthenticationToken) principal).getPrincipal();
	}
}
