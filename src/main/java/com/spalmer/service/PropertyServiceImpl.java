package com.spalmer.service;

import com.spalmer.model.Property;
import com.spalmer.model.PropertyDTO;
import com.spalmer.repository.PropertyRepository;
import com.spalmer.user.model.user.User;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class PropertyServiceImpl implements PropertyService {

	private ModelMapper modelMapper;

	private PropertyRepository propertyRepository;

	@Autowired
	public PropertyServiceImpl(final ModelMapper modelMapper,
	                           final PropertyRepository propertyRepository) {
		this.modelMapper = modelMapper;
		this.propertyRepository = propertyRepository;
	}

	@Override
	public List<PropertyDTO> getProperties(User user) {
		List<Property> properties = propertyRepository.findByUserId(user.getId());

		return properties.stream()
				.map(this::mapPropertyToDTO)
				.collect(Collectors.toList());
	}

	@Override
	public void addProperty(PropertyDTO dto) {
		Property property = modelMapper.map(dto, Property.class);

		propertyRepository.save(property);
	}

	private PropertyDTO mapPropertyToDTO(Property property) {
		return modelMapper.map(property, PropertyDTO.class);
	}
}
