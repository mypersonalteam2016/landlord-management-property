package com.spalmer.service;

import com.spalmer.model.PropertyDTO;
import com.spalmer.user.model.user.User;

import java.util.List;

public interface PropertyService {

	List<PropertyDTO> getProperties(User user);

	void addProperty(PropertyDTO dto);
}
