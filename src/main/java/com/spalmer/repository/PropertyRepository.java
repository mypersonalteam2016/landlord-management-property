package com.spalmer.repository;

import com.spalmer.model.Property;
import com.spalmer.user.model.user.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface PropertyRepository extends MongoRepository<Property, String> {

	List<Property> findByUserId(String id);
}
