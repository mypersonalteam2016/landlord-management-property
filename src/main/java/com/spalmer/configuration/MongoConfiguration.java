package com.spalmer.configuration;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@EnableMongoRepositories("com.spalmer.repository")
public class MongoConfiguration extends AbstractMongoConfiguration {
	@Override
	protected String getDatabaseName() {
		return "housing-tracker-property";
	}

	@Override
	public Mongo mongo() throws Exception {
		return new MongoClient();
	}
}


